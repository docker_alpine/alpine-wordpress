# alpine-wordpress
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-wordpress)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-wordpress)



----------------------------------------
#### [alpine-wordpress](https://hub.docker.com/r/forumi0721/alpine-wordpress/)
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-wordpress/x64)
#### [alpine-wordpress](https://hub.docker.com/r/forumi0721/alpine-wordpress/)
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-wordpress/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Wordpress](https://wordpress.org/)
    - WordPress is web software you can use to create a beautiful website, blog, or app.



----------------------------------------
#### Run

```sh
docker run -d \
           -e DB_HOST=<DB_HOST> \
           -e DB_USER=<DB_USER> \
           -e DB_PASSWD=<DB_PASSWD> \
           -e DB_NAME=<DB_NAME> \
           -p 8080:8080/tcp \
           forumi0721/alpine-wordpress:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8080/](http://localhost:8080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8080/tcp           | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| DB_HOST            | MySQL host address                               |
| DB_USER            | MySQL username                                   |
| DB_PASSWD          | MySQL password                                   |
| DB_NAME            | MySQL db name                                    |

